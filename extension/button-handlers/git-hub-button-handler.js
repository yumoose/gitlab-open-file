class GitHubButtonHandler extends ButtonHandler {
  addButtons(document, editorProtocol, projectDirectory) {
    Array.from(
      document.body.getElementsByClassName('file-info')
    ).forEach((node) => this.addOpenFileButton(node, editorProtocol, projectDirectory));

    const observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.addedNodes && mutation.addedNodes.length > 0) {
          Array.from(mutation.addedNodes).forEach(function(node) {
            (new GitHubButtonHandler).addOpenFileButton(node, editorProtocol, projectDirectory);
          });
        }
      });
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  }

  findLineNumber(node) {
    let lineNumberAttribute = Array.from(node.attributes).find((attr) => attr.name === 'data-line-number')

    if (lineNumberAttribute) {
      return lineNumberAttribute;
    } else {
      let childNodes = Array.from(node.children);

      for(let i = 0; i <= childNodes.length - 1; i++) {
        let childNode = childNodes[i];
        let childLineNumber = this.findLineNumber(childNode);

        if (childLineNumber) {
          return childLineNumber;
        }
      }
    }
  }

  addOpenFileButton(node, editorProtocol, projectDirectory) {
    if (this.isElementNode(node) && this.isFileInfoNode(node) && !this.alreadyHasFileOpenButton(node)) {
      let filename = Array.from(node.childNodes).find((node) => node.tagName === 'A').title;
      let absoluteFilename = projectDirectory + filename;
      let lineNumber = 0;

      if (node.parentNode.nextElementSibling.classList.contains('js-file-content')) {
        let fileContentNode = node.parentNode.nextElementSibling;
        let foundlineNumber = this.findLineNumber(fileContentNode).value;

        if (foundlineNumber) {
          lineNumber = foundlineNumber;
        }
      }

      let href = protocolFor(editorProtocol).buildUrl(absoluteFilename, lineNumber)
      let openFileButton = this.buildFileIconButton(href);

      node.appendChild(openFileButton);
    } else {
      Array.from(
        node.childNodes
      ).forEach((node) => this.addOpenFileButton(node, editorProtocol, projectDirectory));
    }
  }

  isElementNode(node) {
    return node.nodeType === node.ELEMENT_NODE;
  }

  isFileInfoNode(node) {
    return node.classList.contains('file-info');
  }

  alreadyHasFileOpenButton(node) {
    return Array.from(node.childNodes).every((child) => child.getAttribute && child.getAttribute('aria-label') != 'Open file in editor');
  }

  buildFileIconButton(href) {
    let template = document.createElement('template');
    let html = this.buildFileIconButtonHtml(href).trim();
    template.innerHTML = html;

    return template.content.firstChild;
  }

  buildFileIconButtonHtml(href) {
    return `
      <a href="${href}">
        <button type="button" class="btn-link btn-octicon tooltipped tooltipped-nw" aria-label="Open file in editor">
          <svg class="octicon octicon-code" viewBox="0 0 14 16" version="1.1" width="14" height="16" aria-hidden="true">
            <path fill-rule="evenodd" d="M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z"></path>
          </svg>
        </button>
      </a>
    `
  }
}
